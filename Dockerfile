FROM node:lts-fermium

WORKDIR /app

COPY package.json server.js /app/

RUN npm install

EXPOSE 3000

ENTRYPOINT ["node", "server.js"]